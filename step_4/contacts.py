class AddressBook:
    def __init__(self):
        self._employee_address = {
            1: Address("51 Bandera St", "Dnipro", "UA", "49027"),
            2: Address("121 Rose St", "Kharkiv", "UA", "61001"),
            3: Address("2 Krasyva", "Mykolayv", "UA", "32027"),
            4: Address("32  Brativ Rogatynciv St", "Lviv", "UA", "79037"),
            5: Address("100 Nezalegnosti", "Mariupol", "UA", "41000")
        }

    def get_employee_address(self, employee_id):
        address = self._employee_address.get(employee_id)
        if not address:
            raise ValueError("incorrect employee_id")
        return address


class Address:
    def __init__(self, street, city, state, zipcode, street2=''):
        self.street = street
        self.street2 = street2
        self.city = city
        self.state = state
        self.zipcode = zipcode

    def __str__(self):
        lines = [self.street]
        if self.street2:
            lines.append(self.street2)
        lines.append(f"{self.city}, {self.state}  {self.zipcode}")
        return "\n".join(lines)
