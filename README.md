# oop_2_2023_10_31_gr1209

---

[video 1](https://youtu.be/7BgSdAZPrp8) - oop 2, inheritance

[video 2](https://youtu.be/RiUbhEimovM) oop 3, composition, methods: instance, class, static

[video 3](https://youtu.be/iMxheshgEyg) oop 4, exceptions, managed attributes, property
